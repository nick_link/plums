package com.plums.data

import com.plums.data.local.LocalGateway
import com.plums.data.remote.RemoteGateway
import com.plums.main.ui.settings.SettingsUserModel
import io.reactivex.Single

interface Repository {

    fun getColor(): Single<String>

    fun getUserInfo(): Single<SettingsUserModel>

}

class RepositoryImpl(private val remote: RemoteGateway, private val local: LocalGateway) : Repository {
    override fun getUserInfo(): Single<SettingsUserModel> {
        return remote.getUserInfo()
    }

    override fun getColor(): Single<String> {
        return remote.getColors()
    }

}