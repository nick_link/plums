package com.plums.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Url

const val SERVER_BASE_URL = "https://plums.com/"

interface RestApi {

    @GET
    fun getColors(@Url url: String, @HeaderMap headerMap: Map<String, String>): Single<String>

}

fun getHeader(): Map<String, String> {
    return mapOf()
}