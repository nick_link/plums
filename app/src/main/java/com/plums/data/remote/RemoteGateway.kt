package com.plums.data.remote

import com.plums.main.ui.settings.SettingsUserModel
import io.reactivex.Single

interface RemoteGateway{
    fun getColors(): Single<String>

    fun getUserInfo(): Single<SettingsUserModel>
}

class RemoteGatewayImpl(private val api: RestApi): RemoteGateway {
    override fun getUserInfo(): Single<SettingsUserModel> {
        return Single.just(
            SettingsUserModel(
                "https://vignette.wikia.nocookie.net/doom/images/0/03/Iconofsin_large.png/revision/latest?cb=20190110014030",
                "Icon of sin"
            )
        )
    }

    override fun getColors(): Single<String> {
        return api.getColors("", mapOf())
    }
}