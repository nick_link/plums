package com.plums.base

import android.util.Log
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class CompositeViewModel : ViewModel() {
    protected val cd = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        cd.clear()
        Log.e(this.javaClass.simpleName, "onCleared")
    }

    protected fun subscribe(disposable: Disposable) {
        cd.add(disposable)
    }
}