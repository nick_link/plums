package com.plums.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.toolbar_back_title.*

abstract class NavFragment : Fragment() {

    lateinit var navController: NavController

    abstract fun getLayoutId(): Int
    abstract val viewModel: ViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(this.javaClass.simpleName, "onAttach")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        Log.e(this.javaClass.simpleName, "onViewCreated")

        toolbar_back_button?.setOnClickListener { onBackPressed() }
    }

    protected fun onBackPressed() {
        navController.popBackStack()
    }
}