package com.plums.login.email

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import com.plums.main.MainActivity
import kotlinx.android.synthetic.main.fragment_login_email.*

class LoginEmailFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_login_email

    override val viewModel: LoginEmailViewModel by lazy {
        ViewModelProviders.of(this).get(LoginEmailViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_login.setOnClickListener {
            viewModel.doLogin(
                input_email.text.toString(),
                input_password.text.toString()
            )
            navigateToMainActivity()
        }

    }

    private fun navigateToMainActivity() {
        activity?.let {
            startActivity(Intent(it, MainActivity::class.java))
            it.finish()
        }
    }
}