package com.plums.login.signup

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import kotlinx.android.synthetic.main.fragment_login_signup.*

class LoginSignupFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_login_signup

    override val viewModel: LoginSignupViewModel by lazy {
        ViewModelProviders.of(this).get(LoginSignupViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_next.setOnClickListener {
            viewModel.signupNewUser(
                input_name.text?.toString(),
                input_email.text?.toString(),
                input_phone.text?.toString(),
                input_password.text?.toString()
            )
            Toast.makeText(activity, "Creating", Toast.LENGTH_SHORT).show()
        }
    }
}