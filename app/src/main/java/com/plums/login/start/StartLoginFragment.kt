package com.plums.login.start

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import kotlinx.android.synthetic.main.fragment_login_start.*

class StartLoginFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_login_start

    override val viewModel: StartLoginViewModel by lazy {
        ViewModelProviders.of(this).get(StartLoginViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_login.text = "Login"
        button_register.text = "Register"

        button_login.setOnClickListener { navigateToEmailLogin() }
        button_register.setOnClickListener { navigateToSignUp() }
    }


    private fun navigateToEmailLogin() {
        navController.navigate(R.id.action_startLoginFragment_to_loginEmailFragment)
    }

    private fun navigateToSignUp() {
        navController.navigate(R.id.action_startLoginFragment_to_loginSignupFragment)
    }
}