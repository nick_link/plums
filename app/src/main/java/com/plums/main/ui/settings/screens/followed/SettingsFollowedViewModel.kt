package com.plums.main.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SettingsFollowedViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is USER Fragment"
    }
    val text: LiveData<String> = _text
}