package com.plums.main.ui.settings.screens.followed

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import com.plums.main.ui.settings.SettingsFollowedViewModel
import kotlinx.android.synthetic.main.toolbar_back_title.*

class SettingsFollowedFragment : NavFragment() {

    override fun getLayoutId(): Int = R.layout.fragment_settings_followed
    override val viewModel: SettingsFollowedViewModel by lazy {
        ViewModelProviders.of(this).get(SettingsFollowedViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val textView: TextView = view.findViewById(R.id.text_notifications)

        viewModel.text.observe(this, Observer {
            textView.text = it
        })

        toolbar_title.text = "Followed Busineses"
    }
}
