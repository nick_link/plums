package com.plums.main.ui.settings

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.plums.base.CompositeViewModel
import com.plums.data.Repository

class SettingsViewModel(val repo: Repository) : CompositeViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Settings Fragment"
    }
    val text: LiveData<String> = _text

    val settingsUserModel = MutableLiveData<SettingsUserModel>()

    fun getSettingsUserModel() {
        subscribe(
            repo.getUserInfo().subscribe(
            { model ->
                settingsUserModel.value = model
            }, { error ->
                Log.e("TAG", "Error is ->${error.message}")
                })
        )


    }
}

data class SettingsUserModel(val imageUrl: String?, val userName: String?)