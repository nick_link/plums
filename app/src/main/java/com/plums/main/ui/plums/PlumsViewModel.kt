package com.plums.main.ui.plums

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PlumsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Plums Fragment"
    }
    val text: LiveData<String> = _text
}