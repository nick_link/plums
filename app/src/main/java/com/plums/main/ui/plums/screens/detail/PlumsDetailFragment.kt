package com.plums.main.ui.plums.screens.detail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import kotlinx.android.synthetic.main.toolbar_back_title.*

class PlumsDetailFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_plums_detail
    override val viewModel: PlumsDetailViewModel by lazy {
        ViewModelProviders.of(this).get(PlumsDetailViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar_title.text = "Plums details!!!"
    }
}