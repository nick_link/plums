package com.plums.main.ui.calendar

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.plums.R
import kotlinx.android.synthetic.main.view_calendar_item.view.*
import java.util.*

class CalendarAdapter : RecyclerView.Adapter<CalendarAdapter.CalendarViewHolder>() {

    private var list: ArrayList<Int>

    init {
        val thisYear = getYear()
        val thisMonth = getMonth()
        val thisYearMonth = thisYear * 12 + thisMonth

        Log.e("TAG", "Year -> $thisYear  month -> $thisMonth  YearMonth -> $thisYearMonth")
        list = arrayListOf(thisYearMonth - 2, thisYearMonth - 1, thisYearMonth, thisYearMonth + 1, thisYearMonth + 2)
    }

    fun getList(): ArrayList<Int> {
        return list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_calendar_item, parent, false)
        return CalendarViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        holder.onBind(list[position])
    }

    class CalendarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(id: Int) {

            itemView.calendar_view.date = System.currentTimeMillis()

            itemView.calendar_id_text.text = id.toString()

            val year = id / 12
            val month = id % 12

            itemView.calendar_text.text = "Year $year \n Month ${month + 1}"
        }
    }

    private fun getYear(): Int {
        return Calendar.getInstance().get(Calendar.YEAR)
    }

    private fun getMonth(): Int {
        return Calendar.getInstance().get(Calendar.MONTH)
    }
}