package com.plums.main.ui.settings.screens.user

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import com.plums.main.ui.settings.SettingsUserViewModel
import kotlinx.android.synthetic.main.toolbar_back_title.*

class SettingsUserFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_settings_user
    override val viewModel: SettingsUserViewModel by lazy {
        ViewModelProviders.of(this).get(SettingsUserViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val textView: TextView = view.findViewById(R.id.text_notifications)
        viewModel.text.observe(this, Observer {
            textView.text = it
        })

        toolbar_title.text = "User Settings"
    }
}