package com.plums.main.ui.plums

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.plums.R
import com.plums.base.NavFragment
import kotlinx.android.synthetic.main.fragment_plums.*

class PlumsFragment : NavFragment() {

    override fun getLayoutId(): Int = R.layout.fragment_plums
    override val viewModel: PlumsViewModel by lazy { ViewModelProviders.of(this).get(PlumsViewModel::class.java) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val textView: TextView = view.findViewById(R.id.text_home)
        viewModel.text.observe(this, Observer {
            textView.text = it
        })

        plums_to_detail.setOnClickListener { navigateToPlumsDetails() }
    }

    private fun navigateToPlumsDetails() {
        navController.navigate(R.id.action_navigation_plums_to_plumsDetailFragment)
    }
}