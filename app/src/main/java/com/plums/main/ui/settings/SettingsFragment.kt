package com.plums.main.ui.settings

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.Observer
import com.plums.R
import com.plums.base.NavFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_settings.*
import org.koin.android.viewmodel.ext.android.viewModel

class SettingsFragment : NavFragment() {

    override fun getLayoutId(): Int = R.layout.fragment_settings
    override val viewModel: SettingsViewModel by viewModel() //lazy { ViewModelProviders.of(this).get(SettingsViewModel::class.java) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val textView: TextView = view.findViewById(R.id.text_notifications)

        viewModel.text.observe(this, Observer {
            textView.text = it
        })

        viewModel.settingsUserModel.observe(this, Observer { model ->
            Picasso.get().load(model.imageUrl).into(settings_user_image)
            settings_user_text.text = model.userName
        })


        settings_user_parent.setOnClickListener { navigateToUserSettings() }
        settings_followed.setOnClickListener { navigateToFollowed() }

        viewModel.getSettingsUserModel()
    }


    private fun navigateToUserSettings() {
        navController.navigate(R.id.action_navigation_settings_to_navigation_settings_user)
    }

    private fun navigateToFollowed() {
        navController.navigate(R.id.action_navigation_settings_to_navigation_settings_followed)
    }

    private fun navigateToBlocked() {

    }

    private fun navigateToApplication() {

    }

    private fun navigateToFeedback() {

    }

    private fun navigateToTerms() {

    }

    private fun navigateToTutorial() {

    }


}