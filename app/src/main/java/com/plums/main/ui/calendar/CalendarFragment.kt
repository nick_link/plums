package com.plums.main.ui.calendar

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.plums.R
import com.plums.base.NavFragment
import com.plums.base.helpers.OnSnapPositionChangeListener
import com.plums.base.helpers.SnapOnScrollListener
import com.plums.base.helpers.attachSnapHelperWithListener
import kotlinx.android.synthetic.main.fragment_calendar.view.*


class CalendarFragment : NavFragment() {
    override fun getLayoutId(): Int = R.layout.fragment_calendar
    override val viewModel: CalendarViewModel by lazy { ViewModelProviders.of(this).get(CalendarViewModel::class.java) }

    private lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.calendar_recycler
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        val snapHelper = PagerSnapHelper()
        //snapHelper.attachToRecyclerView(recyclerView)

        val adapter = CalendarAdapter()
        recyclerView.adapter = adapter
        recyclerView.scrollToPosition(2)

        recyclerView.attachSnapHelperWithListener(snapHelper, SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL, object :
            OnSnapPositionChangeListener {
            override fun onSnapPositionChange(position: Int) {
                Log.e("TAG", "onScrollStateChanged position -> $position")
                extendList(recyclerView, adapter.getList(), position)
            }
        })

    }


    fun extendList(recyclerView: RecyclerView, list: ArrayList<Int>, position: Int) {
        if (position == 0) {
            list.add(0, firstItemValue(list) - 1)
            recyclerView.safeNotifyAdapter(0)
        } else if (position == list.lastIndex) {
            list.add(list.lastIndex + 1, lastItemValue(list) + 1)
            recyclerView.safeNotifyAdapter(list.lastIndex)
        }
    }

    private fun RecyclerView.safeNotifyAdapter(position: Int) {
        this.post { this.adapter?.notifyItemInserted(position) }
    }

    private fun firstItemValue(list: List<Int>): Int {
        return list[0]
    }

    fun lastItemValue(list: List<Int>): Int {
        return list[list.lastIndex]
    }


}