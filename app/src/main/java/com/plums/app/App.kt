package com.plums.app

import android.app.Application
import com.plums.app.di.dataModule
import com.plums.app.di.modelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin{
            androidContext(this@App)
            modules(dataModule)
            modules(modelsModule)
        }
    }
}