package com.plums.app.di

import com.plums.data.Repository
import com.plums.data.RepositoryImpl
import com.plums.data.local.LocalGateway
import com.plums.data.local.LocalGatewayImpl
import com.plums.data.remote.RemoteGateway
import com.plums.data.remote.RemoteGatewayImpl
import com.plums.data.remote.RestApi
import com.plums.data.remote.SERVER_BASE_URL
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val cacheSize = (10 * 1024 * 1024).toLong()

fun provideDefaultOkhttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        //.addInterceptor(ApiKeyInterceptor())
        .build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(SERVER_BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideRestService(retrofit: Retrofit): RestApi = retrofit.create(RestApi::class.java)

val dataModule = module {
    
    single { provideDefaultOkhttpClient() }
    single { provideRetrofit(get()) }
    single { provideRestService(get()) }

    single<RemoteGateway> { RemoteGatewayImpl(get()) }
    single<LocalGateway> { LocalGatewayImpl() }
    single<Repository> { RepositoryImpl(get(), get()) }
}