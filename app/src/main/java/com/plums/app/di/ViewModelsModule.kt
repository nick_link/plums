package com.plums.app.di

import com.plums.main.ui.settings.SettingsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


val modelsModule = module {

    viewModel { SettingsViewModel(get()) }
//    viewModel { PlumsViewModel(get()) }

}