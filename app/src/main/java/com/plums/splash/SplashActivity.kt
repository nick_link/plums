package com.plums.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.plums.R
import com.plums.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(
            { navigateToMainActivity() },
            1000
        )
    }

    private fun navigateToMainActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
